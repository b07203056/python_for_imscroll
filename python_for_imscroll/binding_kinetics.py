#  Copyright (C) 2020 Tzu-Yu Lee, National Taiwan University
#
#  This file (binding_kinetics.py) is part of python_for_imscroll.
#
#  python_for_imscroll is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  python_for_imscroll is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with python_for_imscroll.  If not, see <https://www.gnu.org/licenses/>.

import json
from collections import namedtuple
import xarray as xr
import numpy as np
from python_for_imscroll import time_series


def list_aois_more_than_two_states(nStates):
    """Returns a list containing aois that visits more than 2 states."""
    # np returns zero index so plus 1
    aoi_list = (np.where(nStates > 2)[0] + 1).tolist()
    return aoi_list


def collect_all_channel_state_info(traces):
    state_info_all = dict()
    StateInfo = namedtuple('StateInfo', ('n_states', 'is_lowest_state_nonzero'))
    for channel in traces.get_channels():
        state_info_all[channel] = []
        for molecule in range(traces.n_traces):
            intensity = traces.get_intensity(channel, molecule)
            viterbi_path = traces.get_viterbi_path(channel, molecule)
            state_means = sorted(set(viterbi_path.data))
            is_lowest_state_nonzero = (abs(state_means[0])
                                       > 2 * np.std(intensity[viterbi_path == state_means[0]]))
            state_info_all[channel].append(StateInfo(len(state_means),
                                                     is_lowest_state_nonzero))
    return state_info_all


def list_multiple_tethers(channel_state_info: list) -> tuple:
    bad_molecules = []
    for molecule, (n_states, is_lowest_state_nonzero) in enumerate(channel_state_info):
        exists_more_than_two_states = n_states > 2
        is_two_state_but_elevated = (n_states == 2 and is_lowest_state_nonzero)
        if exists_more_than_two_states or is_two_state_but_elevated:
            bad_molecules.append(molecule)
    bad_molecules = tuple(bad_molecules)
    return bad_molecules


def list_none_ctl_positions(channel_state_info) -> tuple:
    cond = np.logical_or(channel_state_info.nStates != 1,
                         np.logical_not(channel_state_info.bool_lowest_state_equal_to_zero))
    aoi_list = channel_state_info.AOI[cond].values.tolist()
    return tuple(aoi_list)


def split_data_set_by_specifying_aoi_subset(data, aoi_subset: set):
    whole_aoi_set = set(data.AOI.values)
    complement_subset = whole_aoi_set - aoi_subset
    data_subset = data.sel(AOI=list(aoi_subset))
    data_complement = data.sel(AOI=list(complement_subset))
    return data_subset, data_complement


def get_interval_slices(viterbi_path):
    state_end_index = find_state_end_point(viterbi_path)
    state_start_index = [0] + (state_end_index+1).tolist()
    state_end_index = (state_end_index+1).tolist() + [len(viterbi_path)]
    return (slice(start, stop) for start, stop in zip(state_start_index, state_end_index))


def colocalization_analysis(traces, state_info, binder_channel):
    bad_molecules = []
    for molecule in range(traces.n_traces):
        molecule_state_info = state_info[binder_channel][molecule]
        viterbi_path = traces.get_viterbi_path(binder_channel, molecule)
        is_colocalized = traces.get_is_colocalized(binder_channel, molecule)
        min_state_intensity = viterbi_path.min()
        interval_slices = get_interval_slices(viterbi_path)
        for interval_slice in interval_slices:
            is_zero_state = (viterbi_path[interval_slice.start] == min_state_intensity
                             and not molecule_state_info.is_lowest_state_nonzero)
            if is_zero_state:
                continue

            n_colocalized_time_points = np.count_nonzero(is_colocalized[interval_slice])
            # Viterbi on state must correspond to colocalized event, with 10% tolerance
            reject_condition = n_colocalized_time_points < 0.9 * (interval_slice.stop-interval_slice.start)
            if reject_condition:
                bad_molecules.append(molecule)
                break
    return bad_molecules


def find_state_end_point(state_sequence):
    change_array = np.diff(state_sequence)
    state_end_index = np.nonzero(change_array)[0]
    return state_end_index


def assign_event_time(time_for_each_frame, state_end_index):
    event_time = np.zeros((len(state_end_index) + 2))
    event_time[0] = time_for_each_frame[0]
    event_time[-1] = time_for_each_frame[-1]
    # Assign the time point for events as the mid-point between two points that have different
    # state labels
    for i, i_end_index in enumerate(state_end_index):
        event_time[i + 1] = (time_for_each_frame[i_end_index] +
                             time_for_each_frame[i_end_index + 1]) / 2
    return event_time


def set_up_intervals(event_time):
    intervals = xr.Dataset({'duration': (['AOI', 'interval_number'], np.zeros((1, len(event_time) - 1)))},
                           coords={'interval_number': range(len(event_time) - 1)
                                   })
    intervals['duration'] = xr.DataArray(np.diff(event_time),
                                         dims='interval_number',
                                         coords={'interval_number': intervals.interval_number})
    intervals['start'] = xr.DataArray(event_time[0:-1], dims='interval_number')
    intervals['end'] = xr.DataArray(event_time[1:], dims='interval_number')
    return intervals


def shift_state_number(AOI_data):
    if AOI_data['bool_lowest_state_equal_to_zero']:
        if AOI_data['lowest_state_label'] == 1:
            AOI_data['viterbi_path'].loc[dict(state='label')] += -1
        else:
            raise ValueError('shift_state_number:\nlowest state not equal to 1')
    return AOI_data


def assign_state_number_to_intervals(AOI_data, intervals):
    AOI_data = shift_state_number(AOI_data)
    intervals_state_number = xr.DataArray(np.zeros(len(intervals.interval_number)),
                                          dims='interval_number',
                                          coords={'interval_number': intervals.interval_number})
    for i in intervals['interval_number']:
        interval_slice = slice(intervals['start'].loc[i], intervals['end'].loc[i])
        distinct_state_numbers = set(AOI_data['viterbi_path'].sel(state='label',
                                                                  time=interval_slice).values)
        if len(distinct_state_numbers) == 1:
            intervals_state_number.loc[i] = list(distinct_state_numbers)[0]

        else:
            raise ValueError("""assign_state_number_to_intervals:
            There are more than one state in this interval.""")
    intervals['state_number'] = intervals_state_number
    return intervals


def group_analyzable_aois_into_state_number(analyzable_aois, channel_state_info):
    analyzable_tethers = {}
    for molecule in analyzable_aois:
        state_info = channel_state_info[molecule]
        category = int(state_info.n_states - 1 + state_info.is_lowest_state_nonzero)
        if category in analyzable_tethers:
            analyzable_tethers[category].append(molecule)
        else:
            analyzable_tethers[category] = [molecule]
    return analyzable_tethers


def extract_dwell_time(intervals_list, state):

    out_list = []
    for i_file_intervals in intervals_list:
        for iAOI in i_file_intervals.AOI:
            i_AOI_intervals = i_file_intervals.sel(AOI=iAOI)
            valid_intervals = i_AOI_intervals.where(
                np.logical_not(np.isnan(i_AOI_intervals.duration)),
                drop=True)

            # valid_intervals = valid_intervals.isel(interval_number=slice(1, None))
            if len(valid_intervals.duration) != 0:
                valid_intervals = valid_intervals.assign({'event_observed' :('interval_number', np.ones(len(valid_intervals.interval_number)))})

                valid_intervals['event_observed'][[0, -1]] = 0
                i_dwell = valid_intervals[['duration', 'event_observed']].where(valid_intervals.state_number == state,
                                                                                drop=True)

                i_dwell['event_observed'] = i_dwell['event_observed'].astype(bool)
                i_dwell = i_dwell.reset_index('interval_number')


                out_list.append(i_dwell)

    out = xr.concat(out_list, dim='interval_number')

    return out


def extract_first_binding_time(intervals_list):
    i_file_intervals = intervals_list[0]
    first_intervals = i_file_intervals.sel(interval_number=0)
    left_censored = first_intervals.state_number == 1        
    dwells = first_intervals['duration'].to_dataset()
    dwells['duration'] = xr.where(left_censored, 0, dwells['duration'])
    dwells['event_observed'] = np.logical_not(left_censored)
    return dwells


def get_channel_data(data, channel):
    channel_data = data.sel(channel=channel)
    channel_data = channel_data.assign_coords(channel=[channel])
    return channel_data


def save_all_data(all_data, AOI_categories, path):
    for key, value in all_data.items():
        if key == 'data':
            all_data[key] = value.reset_index('channel_time').to_dict()
        else:
            all_data[key] = value.to_dict()
    collected_data = {'all_data': all_data,
                      'AOI_categories': AOI_categories}
    with open(path, 'w') as file:
        json.dump(collected_data, file)
    return 0


def load_all_data(path):
    with open(path) as file:
        collected_data = json.load(file)
    all_data = collected_data['all_data']
    AOI_categories = collected_data['AOI_categories']
    for key, item in all_data.items():
        if key == 'data':
            all_data[key] = xr.Dataset.from_dict(item).set_index(channel_time=('channel', 'time'))
        else:
            all_data[key] = xr.Dataset.from_dict(item)
    return all_data, AOI_categories
