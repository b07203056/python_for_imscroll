from pathlib import Path
import time_series

def main():
    datadir = Path('/mnt/data/Research/PriA_project/analysis_result/20210928/20210928imscroll/')
    for path in datadir.iterdir():
        if path.stem[-7:] == '_traces':
            filestr = path.stem[:-7]
            npz_path = datadir / f'{filestr}_traces.npz'
            traces = time_series.TimeTraces.from_npz(npz_path)
            traces.to_mat(npz_path.with_suffix('.dat'))


if __name__ == '__main__':
    main()
