from pathlib import Path
from tqdm import tqdm
import python_for_imscroll.image_processing as imp
import python_for_imscroll.drift_correction as dcorr
from python_for_imscroll import mapping
from python_for_imscroll import utils
import python_for_imscroll.time_series as tseries
import gui.image_view as iv

def calculate_colocalization_time_traces(image_group,
                                         frame_range,
                                         target_channel,
                                         binder_channels_map,
                                         aois,
                                         drifter,
                                         thresholds):
    channel_times = {channel: sequence.time for channel, sequence in image_group}
    traces = tseries.TimeTraces(n_traces=len(aois), channels=channel_times)
    for i, image in zip(frame_range[target_channel],
                        image_group.sequences[target_channel][frame_range[target_channel]]):
        time = image_group.sequences[target_channel].time[i]
        drifted_aois = drifter.shift_aois(aois, i)
        intensity = drifted_aois.get_intensity(image)
        traces.set_value('raw_intensity', channel=target_channel,
                         time=time, array=intensity)
        background = drifted_aois.get_background_intensity(image)
        traces.set_value('background', channel=target_channel, time=time, array=background)
        traces.set_value('intensity', channel=target_channel, time=time, array=intensity-background)

    for channel, mapper in binder_channels_map.items():
        channel_obj = imp.Channel(channel, channel)
        ref_aoi_high = []
        ref_aoi_low = []
        aois_list = []
        mapped_aois = mapper.map(aois, to_channel=channel)
        time_list = []
        for i, image in tqdm(zip(frame_range[channel_obj], image_group.sequences[channel_obj][frame_range[channel_obj]]),
                             total=image_group.sequences[channel_obj].length):
            time = image_group.sequences[channel_obj].time[i]
            time_list.append(time)
            drifted_aois = drifter.shift_aois(mapped_aois, i)
            aois_list.append(drifted_aois)
            intensity = drifted_aois.get_intensity(image)
            traces.set_value('raw_intensity', channel=channel_obj, time=time, array=intensity)
            background = drifted_aois.get_background_intensity(image)
            traces.set_value('background', channel=channel_obj, time=time, array=background)
            traces.set_value('intensity', channel=channel_obj, time=time, array=intensity-background)
            ref_aoi_high.append(imp.pick_spots(image, threshold=thresholds[channel_obj][0]))
            ref_aoi_low.append(imp.pick_spots(image, threshold=thresholds[channel_obj][1]))
        is_colocalized = imp._colocalization_from_high_low_spots(aois_list, ref_aoi_high, ref_aoi_low)
        traces.set_is_colocalized(channel_obj, is_colocalized, time_array=time_list)
    return traces


def main():
    data_dir = iv.select_directory_dialog()
    map_dir = iv.select_directory_dialog()
    image_group_dir = iv.select_directory_dialog()
    image_group = imp.ImageGroup(image_group_dir)
    parameter_file_path = iv.open_file_path_dialog()
    parameters = utils.read_excel(parameter_file_path)
    channels_data = utils.read_excel(parameter_file_path, sheet_name='channels')
    channels_data = channels_data.sort_values(by='order').loc[:, ['name', 'map file name']]
    target_channel = imp.Channel(channels_data.name[0], channels_data.name[0])
    binder_channels_map = {row['name']:
                           mapping.Mapper.from_imscroll(map_dir / (row['map file name'] + '.dat'))
                           for i, row in channels_data.iloc[1:, :].iterrows()}
    for _, parameter in parameters.iterrows():
        aoiinfo_path = data_dir / (parameter.filename + '_aoi.dat')
        aois = imp.Aois.from_imscroll_aoiinfo2(aoiinfo_path)
        aois.channel = target_channel[1]

        drift_fit_path = data_dir / (parameter.filename + '_driftfit.dat')
        if drift_fit_path.is_file():
            drifter = dcorr.DriftCorrector.from_imscroll_driftfit(drift_fit_path)
        else:
            drifter = dcorr.DriftCorrector(None)

        thresholds = {imp.Channel('green', 'green'): (parameter.iloc[4], parameter.iloc[5])}
        frame_range_value = range(int(parameter['framestart']-1), int(parameter['frame end']))
        frame_range = {channel: frame_range_value for channel in image_group.channels}
        traces = calculate_colocalization_time_traces(image_group,
                                                      frame_range,
                                                      target_channel,
                                                      binder_channels_map,
                                                      aois,
                                                      drifter,
                                                      thresholds)
        traces.to_npz(data_dir / (parameter.filename + '_traces.npz'))


if __name__ == '__main__':
    main()
