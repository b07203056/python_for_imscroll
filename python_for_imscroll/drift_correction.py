"""This module handles drift corrections."""


import numpy as np
import scipy.signal
import scipy.io as sio
import scipy.interpolate
from tqdm import tqdm
import matplotlib.pyplot as plt
import python_for_imscroll.image_processing as imp

def _make_drift_list_core(coords, frame_range, time=None):
    start_frame, end_frame = frame_range
    diff_coords = np.diff(coords, axis=0, prepend=0)
    mean_diff_coords = np.mean(diff_coords, axis=1)
    mean_displacement = np.cumsum(mean_diff_coords, axis=0)
    filtered_displacement = scipy.signal.savgol_filter(mean_displacement,
                                                       window_length=11,
                                                       polyorder=2,
                                                       axis=0)
    if time is None:
        driftlist = np.zeros((end_frame, 3))
    else:
        # TODO: add test for the with time case
        driftlist = np.zeros((end_frame, 4))
        driftlist[:, 3] = time
    driftlist[:, 0] = np.arange(end_frame)
    driftlist[start_frame+1:end_frame+1, 1:3] = np.diff(filtered_displacement, axis=0)
    return driftlist

def make_drift_list_simple(drift_fit):
    data = drift_fit['data'].item()
    n_aois = int(data[:, 0].max())
    start_frame = int(data[:, 1].min())
    end_frame = int(data[:, 1].max())
    n_frames = end_frame - start_frame + 1
    coords = data[:, 3:5]
    coords = coords.reshape((n_frames, n_aois, 2))
    driftlist = _make_drift_list_core(coords, (start_frame - 1, end_frame))
    return driftlist


class DriftCorrector:
    def __init__(self, driftlist):
        self._driftlist = driftlist

    def shift_aois(self, aois, frame):
        if self._driftlist is None:
            return aois
        start_frame = aois.frame
        drift_range = np.logical_and(self._driftlist[:, 0] >= start_frame + 1,
                                     self._driftlist[:, 0] <= frame)
        drift = self._driftlist[drift_range, 1:].sum(axis=0)[np.newaxis, :]
        new_aois = imp.Aois(aois._coords + drift,
                            frame=frame,
                            width=aois.width,
                            frame_avg=aois.frame_avg,
                            channel=aois.channel)
        return new_aois

    def _time_is_in_correction_range(self, time):
        return self._driftlist[:, 3].min() <= time <= self._driftlist[:, 3].max()

    def shift_aois_by_time(self, aois, time):
        if self._driftlist is None:
            return aois
        if not self._time_is_in_correction_range(time):
            pass
            # TODO: Probably need to add some check back
            # raise ValueError(f'time {time} is not in drift correction range ' +
            #                  f'[{self._driftlist[:, 3].min()}, {self._driftlist[:, 3].max()}]')
        # Assuming the frame number of Aois object is the same as driftlist
        # TODO: think a better way to do this
        self._cum_driftlist = np.copy(self._driftlist)
        self._cum_driftlist[:, 1:3] = np.cumsum(self._cum_driftlist[:, 1:3], axis=0)
        f = scipy.interpolate.interp1d(self._cum_driftlist[:, 3], self._cum_driftlist[:, 1:3],
                                       kind='linear', axis=0, bounds_error=False,
                                       fill_value=(self._cum_driftlist[0, 1:3], self._cum_driftlist[-1, 1:3]))
        drift = f(time)
        new_aois = imp.Aois(aois._coords + drift,
                            frame=aois.frame,  # TODO: need to fix this
                            width=aois.width,
                            frame_avg=aois.frame_avg,
                            channel=aois.channel)
        return new_aois

    @classmethod
    def from_imscroll(cls, path):
        driftlist = sio.loadmat(path)['driftlist'][:, [0, 2, 1]]  # Swap x, y since imscroll image is transposed
        driftlist[:, 0] -= 1
        return cls(driftlist)

    @classmethod
    def from_imscroll_driftfit(cls, path):
        driftfit = sio.loadmat(path)['aoifits']
        driftlist = make_drift_list_simple(driftfit)[:, [0, 2, 1]]  # Swap x, y since imscroll image is transposed
        return cls(driftlist)

    def to_npy(self, path):
        np.save(path, self._driftlist, allow_pickle=False)

    @classmethod
    def from_npy(cls, path):
        driftlist = np.load(path, allow_pickle=False)
        return cls(driftlist)


def drift_detection(image_sequence, frame_range, threshold, ref_aois=None):
    radius = 1.5
    aoi_list = []
    for frame, image in tqdm(zip(frame_range, image_sequence[frame_range]), total=len(frame_range)):
        aoi_list.append(imp.pick_spots(image, threshold=threshold, frame=frame))

    if ref_aois is None:
        ref_aois = aoi_list[0]
    else:
        aoi_list[0] = ref_aois

    first_entry = True
    for i, aois in enumerate(aoi_list[1:], start=1):
        new_ref_aois = aois.remove_aois_far_from_ref(ref_aois, radius)
        if len(new_ref_aois) < 3 and first_entry:
            temp_frame_range = range(frame_range.start, frame_range.start + i)
            temp_aoi_list = aoi_list[:i]
            temp_ref_aois = ref_aois
            first_entry = False
        ref_aois = new_ref_aois
    if not len(ref_aois):
        frame_range = temp_frame_range
        aoi_list = temp_aoi_list
        ref_aois = temp_ref_aois

    last_aois = ref_aois
    drifted_coords = np.zeros((len(frame_range), len(ref_aois), 2))
    for i, aois, image in tqdm((zip(reversed(range(len(frame_range))),
                                    reversed(aoi_list), image_sequence[reversed(frame_range)])), total=len(frame_range)):
        ref_aois = aois.remove_aois_far_from_ref(ref_aois, radius)
        ref_aois.sort_by_ref(last_aois)
        coords = ref_aois.gaussian_refine(image).coords
        drifted_coords[i, :, :] = coords

    plt.plot(np.arange(len(frame_range)), (drifted_coords[:, :, 0].squeeze() - drifted_coords[np.newaxis, 0, :, 0]).squeeze())
    plt.show()
    plt.plot(np.arange(len(frame_range)), (drifted_coords[:, :, 1].squeeze() - drifted_coords[np.newaxis, 0, :, 1]).squeeze())
    plt.show()

    time = image_sequence.time[frame_range]
    driftlist = _make_drift_list_core(drifted_coords, (frame_range.start, frame_range.stop), time=time)
    return DriftCorrector(driftlist)
