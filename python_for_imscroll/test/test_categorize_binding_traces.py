#  Copyright (C) 2020 Tzu-Yu Lee, National Taiwan University
#
#  This file (test_categorize_binding_traces.py) is part of python_for_imscroll.
#
#  python_for_imscroll is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  python_for_imscroll is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with python_for_imscroll.  If not, see <https://www.gnu.org/licenses/>.

from pathlib import Path
import pytest
import xarray.testing
from python_for_imscroll import binding_kinetics
from python_for_imscroll import categorize_binding_traces_script as cbt
from python_for_imscroll import time_series
import python_for_imscroll.image_processing as imp


TEST_DATA_DIR = Path(__file__).parent / 'test_data/20200228/'
def test_get_state_info():
    test_parameter_file_path = TEST_DATA_DIR / '20200228parameterFile.xlsx'
    true_all_data, _ = binding_kinetics.load_all_data(TEST_DATA_DIR / 'L2_all.json')
    true_state_info = true_all_data['state_info']
    traces = time_series.TimeTraces.from_xarray_json(TEST_DATA_DIR / 'L2_data.json')
    state_info = binding_kinetics.collect_all_channel_state_info(traces)
    for channel in set(true_state_info.channel.data):
        for aoi in true_state_info.AOI:
            true_n_states = true_state_info.sel(channel=channel, AOI=aoi).nStates
            true_is_lowest_state_equal_to_zero = true_state_info.sel(channel=channel, AOI=aoi).bool_lowest_state_equal_to_zero
            channel_obj = imp.Channel(channel, channel)
            assert state_info[channel_obj][int(aoi)-1].n_states == true_n_states
            assert (not state_info[channel_obj][int(aoi)-1].is_lowest_state_nonzero) == true_is_lowest_state_equal_to_zero

def test_categorization():
    test_data_path = Path(__file__).parent / 'test_data/20200228/'
    test_parameter_file_path = test_data_path / '20200228parameterFile.xlsx'

    aoi_categories = cbt.categorize_binding_traces(test_parameter_file_path,
                                                   ['L2'],
                                                   test_data_path,
                                                   save_file=False)
    _, true_aoi_categories = binding_kinetics.load_all_data(test_data_path / 'L2_all.json')
    analyzable_key_casted = dict()
    for key, value in aoi_categories['analyzable'].items():
        analyzable_key_casted[str(key)] = [i+1 for i in value]
    for key, value in aoi_categories.items():
        if key == 'analyzable':
            continue
        aoi_categories[key] = [i+1 for i in value]
    aoi_categories['analyzable'] = analyzable_key_casted
    assert aoi_categories == true_aoi_categories
