
from pathlib import Path
import numpy as np
import pytest
import scipy.io as sio
import python_for_imscroll.drift_correction as dcorr
import python_for_imscroll.image_processing as imp

TEST_DATA_DIR = Path(__file__).parent / 'test_data'


def test_make_driftlist_simple():
    driftfit = sio.loadmat(TEST_DATA_DIR / '20200228/L2_driftfit.dat')['aoifits']
    driftlist = dcorr.make_drift_list_simple(driftfit)
    driftlist[:, 0] += 1
    correct_driftlist = sio.loadmat(TEST_DATA_DIR / '20200228/L2_driftlist.dat')['driftlist']
    np.testing.assert_allclose(driftlist, correct_driftlist[:, :3], rtol=0, atol=1e-11)

def test_drift_corrector():
    driftlist = np.ones((10, 3))
    driftlist[:, 0] = np.arange(10)
    driftlist[:, 2] = 2
    driftlist[0, :] = 0
    drifter = dcorr.DriftCorrector(driftlist)
    np.testing.assert_equal(drifter._driftlist, driftlist)
    arr = np.random.uniform(0, 100, (100, 2))

    # Starting from the first frame
    aois = imp.Aois(arr, frame=0, frame_avg=1, width=7, channel='blue')
    for frame in range(10):
        new_aois = drifter.shift_aois(aois, frame)
        assert isinstance(new_aois, imp.Aois)
        assert new_aois.frame == frame
        assert new_aois.frame_avg == aois.frame_avg
        assert new_aois.width == aois.width
        assert new_aois.channel == aois.channel
        correct_arr = arr[:, :]
        correct_arr[:, 0] += frame
        correct_arr[:, 1] += frame*2
        np.testing.assert_allclose(new_aois._coords, correct_arr)

    # Starting from other frames
    aois = imp.Aois(arr, frame=2, frame_avg=1, width=7, channel='blue')
    for frame in range(2, 10):
        new_aois = drifter.shift_aois(aois, frame)
        assert isinstance(new_aois, imp.Aois)
        assert new_aois.frame == frame
        assert new_aois.frame_avg == aois.frame_avg
        assert new_aois.width == aois.width
        assert new_aois.channel == aois.channel
        correct_arr = arr[:, :]
        correct_arr[:, 0] += frame-2
        correct_arr[:, 1] += (frame-2)*2
        np.testing.assert_allclose(new_aois._coords, correct_arr)


def test_batch_shift_aois():
    aoiinfo_path = TEST_DATA_DIR / '20200228/L2_aoi.dat'
    aois = imp.Aois.from_imscroll_aoiinfo2(aoiinfo_path)
    drifter = dcorr.DriftCorrector.from_imscroll(TEST_DATA_DIR / '20200228/L2_driftlist.dat')
    correct_xy = sio.loadmat(TEST_DATA_DIR / '20200228/L2_batch_shift.mat')['xy'] - 1
    for i in range(850):
        print(aois.frame)
        new_aois = drifter.shift_aois(aois, i)
        np.testing.assert_allclose(new_aois._coords, correct_xy[:, :, i])


def test_drift_corrector_load_save_npy():
    drifter = dcorr.DriftCorrector.from_imscroll(TEST_DATA_DIR / '20200228/L2_driftlist.dat')
    save_path = TEST_DATA_DIR / 'save_path'
    drifter.to_npy(save_path / 'driftlist.npy')
    loaded_drifter = dcorr.DriftCorrector.from_npy(save_path / 'driftlist.npy')
    np.testing.assert_equal(loaded_drifter._driftlist, drifter._driftlist)


def test_shift_aois_by_time():
    driftlist = np.tile(np.arange(20)[:, np.newaxis], (1, 4))
    driftlist[1:, 1:3] = np.diff(driftlist[:, 1:3], axis=0)
    drifter = dcorr.DriftCorrector(driftlist)
    aois = imp.Aois(np.zeros((1, 2)), 0)
    for t in np.arange(0, 19.05, 0.05):
        new_aois = drifter.shift_aois_by_time(aois, t)
        np.testing.assert_equal(new_aois.coords, aois.coords + t)

    # Check for exception when the given time is outside of the correction
    # range
    # for t in [-1, -0.1, 19.05, 20]:
    #     with pytest.raises(ValueError) as exception_info:
    #         new_aois = drifter.shift_aois_by_time(aois, t)
    #     assert str(exception_info.value) == f'time {t} is not in drift correction range [0, 19]'
