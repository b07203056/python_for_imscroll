from pathlib import Path
import re
import numpy as np
import pytest
from python_for_imscroll import time_series
from python_for_imscroll import imscrollIO
from python_for_imscroll import image_processing as imp

TEST_DATA_DIR=Path(__file__).parent / 'test_data'

def test_time_traces():
    traces = time_series.TimeTraces.from_xarray_json_all_file(TEST_DATA_DIR / '20200228/L2_all.json')
    n_frames = traces.get_n_traces()
    assert isinstance(n_frames, int)

    traces = time_series.TimeTraces(channels={imp.Channel('blue', 'blue'): np.arange(10),
                                              imp.Channel('green', 'green'): np.arange(20)+0.5}, n_traces=10)
    assert traces.n_traces == 10

    channels = traces.get_channels()
    assert isinstance(channels, list)
    for i in channels:
        assert isinstance(i, imp.Channel)
    assert channels == [imp.Channel('blue', 'blue'), imp.Channel('green', 'green')]


    expected_exception_str = 'Input array length (11) does not match n_traces (10).'
    with pytest.raises(ValueError, match=re.escape(expected_exception_str)) as exception:
        traces.set_value('intensity', channel=imp.Channel('blue', 'blue'), time=0, array=np.arange(11))

    fake_data = np.arange(10)
    for i in range(10):
        traces.set_value('intensity', channel=imp.Channel('blue', 'blue'), time=0+i, array=fake_data+i*2)
    correct_data = np.arange(0, 20, 2)
    for i in range(10):
        intensity = traces.get_intensity(imp.Channel('blue', 'blue'), i)
        np.testing.assert_equal(intensity, correct_data+i)

    traces.set_value('is_colocalized', channel=imp.Channel('blue', 'blue'), time=0+i, array=fake_data+i*2)

    for i in range(10):
        traces.set_value('intensity', channel=imp.Channel('green', 'green'), time=0.5+i, array=100+fake_data+i*2)
    path = TEST_DATA_DIR / 'save_path/traces'
    assert isinstance(traces.get_time(imp.Channel('green', 'green')), np.ndarray)
    assert isinstance(traces.get_time(imp.Channel('blue', 'blue')), np.ndarray)
    traces.to_npz(path)
    new_traces = time_series.TimeTraces.from_npz(path.with_suffix('.npz'))
    assert traces.n_traces == new_traces.n_traces
    for i in range(new_traces.n_traces):
        np.testing.assert_equal(new_traces.get_intensity(imp.Channel('blue', 'blue'), i),
                                traces.get_intensity(imp.Channel('blue', 'blue'), i))
        np.testing.assert_equal(new_traces.get_intensity(imp.Channel('green', 'green'), i),
                                traces.get_intensity(imp.Channel('green', 'green'), i))
        np.testing.assert_equal(new_traces.get_time(imp.Channel('blue', 'blue')),
                                traces.get_time(imp.Channel('blue', 'blue')))
        np.testing.assert_equal(new_traces.get_time(imp.Channel('green', 'green')),
                                traces.get_time(imp.Channel('green', 'green')))

    assert traces.has_variable(imp.Channel('blue', 'blue'), 'is_colocalized')
    assert not traces.has_variable(imp.Channel('blue', 'blue'), 'is_colocalizeds')
    assert not traces.has_variable(imp.Channel('green', 'green'), 'is_colocalized')
    assert isinstance(new_traces.get_time(imp.Channel('green', 'green')), np.ndarray)
    assert isinstance(new_traces.get_time(imp.Channel('blue', 'blue')), np.ndarray)


def test_load_xarray_json_file():
    datapath = TEST_DATA_DIR / '20200228/L2_data.json'
    xr_data = imscrollIO.load_data_from_json(datapath)
    channel_dict = {'blue': imp.Channel('blue', 'blue'),
                    'green': imp.Channel('green', 'green')}
    n_molecules = len(xr_data.AOI)
    traces = time_series.TimeTraces.from_xarray_json(datapath)
    assert traces.n_traces == n_molecules
    for channel_str, channel in channel_dict.items():
        time = xr_data.time.sel(channel=channel_str)
        np.testing.assert_equal(traces.get_time(channel), time)
        assert isinstance(traces.get_time(channel), np.ndarray)
        for i_molecule in range(n_molecules):
            intensity = xr_data['intensity'].sel(channel=channel_str, AOI=i_molecule+1)
            np.testing.assert_equal(traces.get_intensity(channel, i_molecule), intensity)
            viterbi_path = xr_data['viterbi_path'].sel(channel=channel_str, AOI=i_molecule+1, state='position')
            np.testing.assert_equal(traces.get_viterbi_path(channel, i_molecule), viterbi_path)
            if channel_str == 'green':
                is_colocalized = (xr_data['interval_traces'].sel(channel=channel_str, AOI=i_molecule+1).data % 2).astype(bool)
                np.testing.assert_equal(traces.get_is_colocalized(channel, i_molecule), is_colocalized)
        if channel_str == 'blue':
            assert not traces.has_variable(channel, 'is_colocalized')
